package com.magicplugin

import com.magicplugin.listeners.ArrowRainListener
import com.magicplugin.longtasks.ManaRecoverTask
import com.magicplugin.longtasks.ScoreBoardPrinter
import com.magicplugin.longtasks.SpellListTask
import com.magicplugin.spells.effective.EffectiveSpellFactory
import com.magicplugin.spells.effective.EffectiveSpellManager
import com.magicplugin.spells.effective.castable.AreaHealSpell
import com.magicplugin.spells.effective.castable.MeteoritSpell
import com.magicplugin.spells.effective.instant.ArrowRainSpell
import com.magicplugin.spells.effective.instant.FireLineSpell
import com.magicplugin.spells.effective.instant.WallSpell
import com.magicplugin.spells.passive.PassiveManager
import com.magicplugin.spells.passive.implemented.FireRoundPassive
import com.magicplugin.spells.passive.implemented.WalkWaterPassive
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

import java.util.ArrayList


class PluginRoot : JavaPlugin() {

    private val activeSpells = arrayOf<EffectiveSpellFactory>(WallSpell.spellFactory, MeteoritSpell.spellFactory, ArrowRainSpell.spellFactory, AreaHealSpell.spellFactory)
    private val activeSpells2 = arrayOf<EffectiveSpellFactory>(FireLineSpell.spellFactory)

    override fun onEnable() {
        val effectiveSpellManager = EffectiveSpellManager.getInstance(Material.STICK, Material.STICK, activeSpells)
        val effectiveSpellManager2 = EffectiveSpellManager.getInstance(Material.GOLDEN_SWORD, Material.GOLDEN_SWORD, activeSpells2)

        //Custom effects Listeners
        server.pluginManager.registerEvents(ArrowRainListener(), this)

        server.pluginManager.registerEvents(effectiveSpellManager, this)
        server.pluginManager.registerEvents(effectiveSpellManager2, this)
        server.pluginManager.registerEvents(ScoreBoardPrinter(), this)
        server.pluginManager.registerEvents(PassiveManager(Material.DIAMOND, Material.DIAMOND, WalkWaterPassive.spellFactory), this)
        server.pluginManager.registerEvents(PassiveManager(Material.ARROW, Material.ARROW, FireRoundPassive.spellFactory), this)

        ManaRecoverTask(this).runTaskTimer(this, 0, 20)

        val managerList = ArrayList<EffectiveSpellManager>()
        managerList.add(effectiveSpellManager)
        managerList.add(effectiveSpellManager2)
        SpellListTask(this, managerList).runTaskTimer(this, 0, 10)
    }

    override fun onDisable() {

    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        return if (command.name.equals("testcmd", ignoreCase = true)) {
            sender.server.broadcastMessage("TEST CMD SUCCESS")
            true
        } else
            false
    }

}
