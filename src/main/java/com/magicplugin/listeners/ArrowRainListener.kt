package com.magicplugin.listeners

import com.magicplugin.spells.effective.instant.ArrowRainSpell
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.ProjectileHitEvent

class ArrowRainListener : Listener {

    @EventHandler
    fun onHit(event: ProjectileHitEvent) {
        if (event.entity.getMetadata(ArrowRainSpell.EXPLOSIVE).size > 0 && event.entity.getMetadata(ArrowRainSpell.EXPLOSIVE)[0].asInt() > 0) {
            event.entity.world.createExplosion(event.entity.location, event.entity.getMetadata(ArrowRainSpell.EXPLOSIVE)[0].asInt().toFloat())
            event.entity.remove()
        }
    }
}
