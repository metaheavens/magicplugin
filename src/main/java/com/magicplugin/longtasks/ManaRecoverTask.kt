package com.magicplugin.longtasks

import com.magicplugin.spells.effective.EffectiveSpellManager
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scoreboard.Score

class ManaRecoverTask(private val plugin: JavaPlugin) : BukkitRunnable() {

    override fun run() {
        val onlinePlayers = plugin.server.onlinePlayers

        for (player in onlinePlayers) {
            val mana = player.scoreboard.getObjective("Spell info")!!.getScore("Mana")
            if (mana.score < 100)
                mana.score = mana.score + 1
            else
                mana.score = 100
        }
    }
}
