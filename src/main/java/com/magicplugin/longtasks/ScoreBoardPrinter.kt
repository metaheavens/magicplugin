package com.magicplugin.longtasks

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.scoreboard.*

import java.util.UUID

class ScoreBoardPrinter : Listener {

    @EventHandler
    fun onConnect(event: PlayerJoinEvent) {
        val scoreboardManager = Bukkit.getScoreboardManager()
        val scoreboard = scoreboardManager!!.newScoreboard
        val objective = scoreboard.registerNewObjective("Spell info", "dummy", "Spell info")
        val mana = objective.getScore("Mana")
        mana.score = 100

        val cost = objective.getScore("Cost")
        cost.score = 10

        objective.displaySlot = DisplaySlot.SIDEBAR

        event.player.scoreboard = scoreboard
    }
}
