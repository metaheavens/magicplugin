package com.magicplugin.longtasks

import com.magicplugin.spells.effective.EffectiveSpellManager
import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable

import java.util.ArrayList

class SpellListTask(private val plugin: JavaPlugin, private val managers: ArrayList<EffectiveSpellManager>) : BukkitRunnable() {

    override fun run() {
        val onlinePlayers = plugin.server.onlinePlayers

        for (player in onlinePlayers) {
            for (manager in managers) {
                if (player.inventory.itemInMainHand.type == manager.castMaterial) {
                    val textComponent = TextComponent(manager.spellListString(player))
                    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, textComponent)
                    return
                }
            }
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent(""))
        }
    }
}
