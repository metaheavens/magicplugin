package com.magicplugin.spells

import org.bukkit.event.player.PlayerEvent
import org.bukkit.scoreboard.Score

abstract class AbstractSpell {

    protected abstract val manaPrice: Int
    protected abstract fun cast(event: PlayerEvent)

    protected fun reduceMana(event: PlayerEvent): Boolean {
        return if (event.player.scoreboard.getObjective("Spell info")!!.getScore("Mana").score >= manaPrice) {
            val mana = event.player.scoreboard.getObjective("Spell info")!!.getScore("Mana")
            mana.score = mana.score - manaPrice
            true
        } else
            false
    }

    protected open fun beforeCast() {}
}
