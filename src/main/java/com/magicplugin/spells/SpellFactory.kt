package com.magicplugin.spells

/**
 * Created by metaheavens on 15/06/17.
 */
abstract class SpellFactory {
    abstract val spellName: String

    abstract val spellCost: Int?
}
