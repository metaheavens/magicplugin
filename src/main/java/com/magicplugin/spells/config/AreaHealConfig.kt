package com.magicplugin.spells.config

/**
 * Created by metaheavens on 10/06/17.
 */
object AreaHealConfig {
    val MAXDISTANCE = 50
    val REPETITION = 5
    val TIMEBETWEEN = 30
    val SPREADSIZE = 15
    val CASTTIME = 5
    val MANAPRICE = 20
    val HEALTHGIVEN = 3
    val NAME = "Area Heal"
}
