package com.magicplugin.spells.config

/**
 * Created by pamartn on 13/06/17.
 */
object ArrowRainConfig {

    val NAME = "Arrow Rain"
    val EXPLOSIVE = "explosive_arrow"
    val WIDTH = 1
    val EXPLOSION_RADIUS = 3
    val MANAPRICE = 20
}
