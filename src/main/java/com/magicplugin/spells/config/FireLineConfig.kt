package com.magicplugin.spells.config

/**
 * Created by metaheavens on 10/06/17.
 */
object FireLineConfig {
    val NAME = "Fire Line"
    val MANAPRICE = 5
}
