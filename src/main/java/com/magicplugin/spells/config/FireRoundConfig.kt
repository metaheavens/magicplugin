package com.magicplugin.spells.config

/**
 * Created by metaheavens on 10/06/17.
 */
object FireRoundConfig {
    val MANAPRICE = 1
    val RADIUS = 5
    val NAME = "Fire Round"
}
