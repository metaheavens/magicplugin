package com.magicplugin.spells.config

object MeteoritConfig {
    val HEIGHOFFALLING = 30
    val MAXDISTANCE = 50
    val BALLNUMBER = 20
    val REPETITION = 4
    val TIMEBETWEEN = 10
    val SPREADSIZE = 15
    val CASTTIME = 5
    val MANAPRICE = 20
    val NAME = "Meteorit"
}
