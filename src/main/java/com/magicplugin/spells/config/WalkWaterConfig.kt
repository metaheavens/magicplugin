package com.magicplugin.spells.config

object WalkWaterConfig {
    val NAME = "Walk Water"
    val MANAPRICE = 1
    val RADIUS = 2
}
