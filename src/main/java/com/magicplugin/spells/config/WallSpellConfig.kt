package com.magicplugin.spells.config

/**
 * Created by metaheavens on 10/06/17.
 */
object WallSpellConfig {
    val NAME = "Wall"
    val MANAPRICE = 5
}
