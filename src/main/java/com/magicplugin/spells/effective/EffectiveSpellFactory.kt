package com.magicplugin.spells.effective

import com.magicplugin.spells.SpellFactory
import org.bukkit.event.player.PlayerEvent

/**
 * Created by metaheavens on 15/06/17.
 */
abstract class EffectiveSpellFactory : SpellFactory() {
    abstract fun launchSpell(event: PlayerEvent): EffectiveSpell
}
