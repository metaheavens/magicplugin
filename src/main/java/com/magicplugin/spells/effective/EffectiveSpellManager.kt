package com.magicplugin.spells.effective

import com.magicplugin.PluginRoot
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.metadata.MetadataValue
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scoreboard.Score

import java.util.Arrays
import java.util.Optional

/**
 * Created by pamartn on 6/7/17.
 */
class EffectiveSpellManager private constructor(val castMaterial: Material, private val changeItem: Material, private val spells: Array<EffectiveSpellFactory>, id: Int) : Listener {
    private val plugin: JavaPlugin = JavaPlugin.getProvidingPlugin(PluginRoot::class.java)
    private val key: String = "spell_id$id"

    private fun rotatePlayerSpell(player: Player) {
        setPlayerSpellId(player, (getPlayerSpellId(player) + 1) % spells.size)
    }

    private fun getActualSpellName(player: Player): String {
        val spell = spells[getPlayerSpellId(player)]
        return spell.spellName
    }

    @EventHandler
    fun onPlayerEvent(event: PlayerInteractEvent) {
        val leftClick = event.action == Action.LEFT_CLICK_AIR || event.action == Action.LEFT_CLICK_BLOCK
        val rightClick = event.action == Action.RIGHT_CLICK_AIR || event.action == Action.RIGHT_CLICK_BLOCK

        if (!isPlayerSpellIdDefined(event.player)) {
            setPlayerSpellId(event.player, 0)
        }
        if (event.item != null && event.item!!.type == castMaterial && leftClick) {
            val spell_id = getPlayerSpellId(event.player)
            spells[spell_id].launchSpell(event)
        } else if (event.item != null && event.item!!.type == changeItem && rightClick) {
            rotatePlayerSpell(event.player)
            changeCost(event.player)
        }
    }

    private fun setPlayerSpellId(player: Player, i: Int) {
        player.setMetadata(key, FixedMetadataValue(plugin, i))
    }

    private fun isPlayerSpellIdDefined(player: Player): Boolean {
        return player.getMetadata(key).size != 0
    }

    private fun getPlayerSpellId(player: Player): Int {
        val metadatas = player.getMetadata(key)
        for (metadata in metadatas) {
            if (metadata.owningPlugin!!.name == plugin.name) {
                return metadata.asInt()
            }
        }
        return 0
    }

    fun spellListString(player: Player): String {
        if (spells.isEmpty()) return "error"

        return spells.map { spell ->
            if (spell.spellName == getActualSpellName(player)) "" + ChatColor.RED + spell.spellName + ChatColor.BLACK
            else "" + ChatColor.BLACK + spell.spellName + ChatColor.BLACK
        }.reduce { a, b -> "$a - $b" }
    }

    private fun changeCost(player: Player) {
        val spellCost = spells[getPlayerSpellId(player)].spellCost
        val cost = player.scoreboard.getObjective("Spell info")!!.getScore("Cost")
        cost.score = spellCost!!
    }

    companion object {
        private var counter = 0

        fun getInstance(castItem: Material, changeItem: Material, spells: Array<EffectiveSpellFactory>): EffectiveSpellManager {
            counter++
            return EffectiveSpellManager(castItem, changeItem, spells, counter)
        }
    }
}
