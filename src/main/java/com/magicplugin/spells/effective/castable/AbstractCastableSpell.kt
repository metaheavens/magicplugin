package com.magicplugin.spells.effective.castable

import com.magicplugin.PluginRoot
import com.magicplugin.spells.AbstractSpell
import com.magicplugin.spells.effective.EffectiveSpell
import com.magicplugin.spells.effective.castable.tasks.CastTimeTask
import org.bukkit.event.player.PlayerEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable

abstract class AbstractCastableSpell protected constructor(val event: PlayerEvent) : EffectiveSpell() {

    internal abstract val castTime: Int

    init {
        cast(event)
    }

    internal abstract fun getRunnableSpell(callback: BukkitRunnable): BukkitRunnable

    override fun cast(event: PlayerEvent) {
        if (reduceMana(event)) {
            beforeCast()
            val callback = object : BukkitRunnable() {
                override fun run() {
                    afterCast()
                }
            }
            val castTask = CastTimeTask(event.player, event.player.location.clone(), castTime, TIMEBETWEENTICKS, getSpellRunnableWithAfterCast(callback), callback)
            castTask.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
        }
    }

    private fun getSpellRunnableWithAfterCast(aftercallback: BukkitRunnable): BukkitRunnable {
        return JoinedRunnable(getRunnableSpell(aftercallback))
    }

    protected open fun afterCast() {}

    private inner class JoinedRunnable internal constructor(private val runnableSpell: BukkitRunnable) : BukkitRunnable() {

        override fun run() {
            runnableSpell.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
        }
    }

    companion object {
        private val TIMEBETWEENTICKS = 10

        val spellFactory: CastableSpellFactory
            @Throws(Exception::class)
            get() = throw Exception("AbstractCastablSpell is not instanciable, he does not have factory.")
    }
}
