package com.magicplugin.spells.effective.castable

import com.magicplugin.spells.AbstractSpell
import com.magicplugin.spells.effective.EffectiveSpell
import com.magicplugin.spells.effective.castable.tasks.AreaHealTask
import com.magicplugin.spells.config.AreaHealConfig
import org.bukkit.event.player.PlayerEvent
import org.bukkit.scheduler.BukkitRunnable

class AreaHealSpell private constructor(event: PlayerEvent) : AbstractCastableSpell(event) {

    protected override val manaPrice: Int
        get() = AreaHealConfig.MANAPRICE

    internal override val castTime: Int
        get() = AreaHealConfig.CASTTIME

    internal override fun getRunnableSpell(callback: BukkitRunnable): BukkitRunnable {
        return AreaHealTask(AreaHealConfig.REPETITION, event, callback)
    }

    companion object {

        val spellFactory: CastableSpellFactory
            get() = object : CastableSpellFactory() {

                override val spellName: String
                    get() = AreaHealConfig.NAME

                override val spellCost: Int?
                    get() = AreaHealConfig.MANAPRICE

                override fun launchSpell(event: PlayerEvent): AbstractCastableSpell {
                    return AreaHealSpell(event)
                }
            }
    }
}
