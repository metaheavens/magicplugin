package com.magicplugin.spells.effective.castable

import com.magicplugin.spells.effective.EffectiveSpellFactory
import org.bukkit.event.player.PlayerEvent

/**
 * Created by pamartn on 6/7/17.
 */
abstract class CastableSpellFactory : EffectiveSpellFactory() {
    abstract override fun launchSpell(event: PlayerEvent): AbstractCastableSpell
}
