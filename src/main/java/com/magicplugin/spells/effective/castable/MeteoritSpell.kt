package com.magicplugin.spells.effective.castable

import com.magicplugin.spells.config.MeteoritConfig
import com.magicplugin.spells.effective.castable.tasks.MeteoritTask
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.player.PlayerEvent
import org.bukkit.scheduler.BukkitRunnable

import java.util.HashSet

class MeteoritSpell private constructor(event: PlayerEvent) : AbstractCastableSpell(event) {

    private var altertBlock: Block? = null

    override val manaPrice: Int
        get() = MeteoritConfig.MANAPRICE

    override val castTime: Int
        get() = MeteoritConfig.CASTTIME

    override fun getRunnableSpell(callback: BukkitRunnable): BukkitRunnable {
        return MeteoritTask(MeteoritConfig.REPETITION, event, callback)
    }

    override fun beforeCast() {
        val targetBlock = event.player.getTargetBlock(null as HashSet<Material>?, MeteoritConfig.MAXDISTANCE)
        targetBlock.type = Material.GLOWSTONE
        altertBlock = targetBlock
    }

    override fun afterCast() {
        altertBlock!!.type = Material.AIR
    }

    companion object {

        val spellFactory: CastableSpellFactory
            get() = object : CastableSpellFactory() {

                override val spellName: String
                    get() = MeteoritConfig.NAME

                override val spellCost: Int?
                    get() = MeteoritConfig.MANAPRICE

                override fun launchSpell(event: PlayerEvent): AbstractCastableSpell {
                    return MeteoritSpell(event)
                }
            }
    }
}