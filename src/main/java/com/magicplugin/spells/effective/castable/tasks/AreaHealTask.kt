package com.magicplugin.spells.effective.castable.tasks

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.AreaHealConfig
import com.magicplugin.tools.SquareZone
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.player.PlayerEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.util.Vector

import java.util.*

class AreaHealTask(val repetition: Int,
                   val event: PlayerEvent,
                   val centerFallingLocation: Location,
                   val callback: BukkitRunnable,
                   val spellZone: SquareZone,
                   val blockToMaterial: HashMap<Block, Material>,
                   val zoneIsGlowstoned: Boolean) : BukkitRunnable() {

    constructor(repetition: Int, event: PlayerEvent, callback: BukkitRunnable) :
            this(repetition,
                    event,
                    initCenterFallingLocation(event),
                    callback,
                    getInitSpellZone(initCenterFallingLocation(event)),
                    HashMap(),
                    false)

    override fun run() {
        val magicplugin = JavaPlugin.getProvidingPlugin(PluginRoot::class.java)
        val onlinePlayers = magicplugin.server.onlinePlayers

        if (!zoneIsGlowstoned) glowstoneTheZone()

        for (player in onlinePlayers) {
            if (spellZone.isInZone(player.location)) {
                if (player.health + AreaHealConfig.HEALTHGIVEN <= 20)
                    player.health = player.health + AreaHealConfig.HEALTHGIVEN
                else
                    player.health = 20.0
            }
        }

        if (repetition > 1) {
            AreaHealTask(repetition - 1, event, centerFallingLocation, callback, spellZone, blockToMaterial, true).runTaskLater(JavaPlugin.getProvidingPlugin(PluginRoot::class.java), AreaHealConfig.TIMEBETWEEN.toLong())
        } else {
            removeGlowstone()
            callback.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
        }
    }

    private fun glowstoneTheZone() {
        val surfaceToGlow = spellZone.surface

        for (block in surfaceToGlow) {
            if (block.type != Material.GLOWSTONE) {
                blockToMaterial[block] = block.type
            }
            block.type = Material.GLOWSTONE
        }
    }

    private fun removeGlowstone() {
        for ((key, value) in blockToMaterial) {
            key.type = value
        }
    }

    companion object {
        private fun initCenterFallingLocation(event: PlayerEvent): Location {
            return event.player.getTargetBlock(null as HashSet<Material>?, AreaHealConfig.MAXDISTANCE).location.clone()
        }

        private fun getInitSpellZone(centerFallingLocation: Location): SquareZone {
            val firstLocation = centerFallingLocation.clone().add(Vector(AreaHealConfig.SPREADSIZE / 2, 0, AreaHealConfig.SPREADSIZE / 2))
            val secondLocation = centerFallingLocation.clone().subtract(Vector(AreaHealConfig.SPREADSIZE / 2, 0, AreaHealConfig.SPREADSIZE / 2))
            return SquareZone(firstLocation, secondLocation)
        }
    }
}

