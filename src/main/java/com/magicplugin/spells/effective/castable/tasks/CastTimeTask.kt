package com.magicplugin.spells.effective.castable.tasks

import com.magicplugin.PluginRoot
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable


class CastTimeTask(private val p: Player, private val initLocation: Location, private val tickleft: Int, private val timebetweentick: Int, private val spellCallback: BukkitRunnable, private val uncastCallback: BukkitRunnable) : BukkitRunnable() {

    override fun run() {
        p.sendTitle(String(CharArray(tickleft)).replace("\u0000", ChatColor.GREEN.toString() + "."), "", 0, 10, 0)
        val pLoc = p.location
        val xIsGood = initLocation.blockX - 1 < pLoc.blockX && initLocation.blockX + 1 > pLoc.blockX
        val yIsGood = initLocation.blockY - 1 < pLoc.blockY && initLocation.blockY + 1 > pLoc.blockY
        if (xIsGood && yIsGood) {
            if (tickleft == 0) {
                spellCallback.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
            } else
                CastTimeTask(p, initLocation, tickleft - 1, timebetweentick, spellCallback, uncastCallback).runTaskLater(JavaPlugin.getProvidingPlugin(PluginRoot::class.java), timebetweentick.toLong())
        } else {
            uncastCallback.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
        }
    }
}
