package com.magicplugin.spells.effective.castable.tasks

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.MeteoritConfig
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Entity
import org.bukkit.entity.EntityType
import org.bukkit.event.player.PlayerEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.util.Vector

import java.util.HashSet

class MeteoritTask : BukkitRunnable {

    private val centerFallingLocation: Location
    private val callback: BukkitRunnable
    private val repetition: Int
    private val event: PlayerEvent

    private val randomFallingLocation: Location
        get() {
            val newLocation = centerFallingLocation.clone().add(Vector(0, MeteoritConfig.HEIGHOFFALLING, 0))
            val x = (Math.random() * MeteoritConfig.SPREADSIZE).toInt() - MeteoritConfig.SPREADSIZE / 2
            val z = (Math.random() * MeteoritConfig.SPREADSIZE).toInt() - MeteoritConfig.SPREADSIZE / 2
            newLocation.add(Vector(x, 0, z)).direction = Vector(0, 0, 0)
            return newLocation
        }

    constructor(repetition: Int, event: PlayerEvent, callback: BukkitRunnable) {
        this.repetition = repetition
        this.event = event
        this.callback = callback
        this.centerFallingLocation = initCenterFallingLocation()
    }

    private constructor(repetition: Int, event: PlayerEvent, centerFallingLocation: Location, callback: BukkitRunnable) {
        this.repetition = repetition
        this.callback = callback
        this.event = event
        this.centerFallingLocation = centerFallingLocation
    }

    override fun run() {
        for (i in 0 until MeteoritConfig.BALLNUMBER) {
            val randomLocation = randomFallingLocation
            throwMeteorit(randomLocation)
        }

        if (repetition > 1) {
            MeteoritTask(repetition - 1, event, centerFallingLocation, callback).runTaskLater(JavaPlugin.getProvidingPlugin(PluginRoot::class.java), MeteoritConfig.TIMEBETWEEN.toLong())
        } else {
            callback.runTask(JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
        }
    }

    private fun initCenterFallingLocation(): Location {
        return event.player.getTargetBlock(null as HashSet<Material>?, MeteoritConfig.MAXDISTANCE).location.clone()
    }

    private fun throwMeteorit(fallFromLocation: Location) {
        val fireball = event.player.world.spawnEntity(fallFromLocation, EntityType.FIREBALL)
        fireball.velocity = Vector(0, -1, 0)
    }
}
