package com.magicplugin.spells.effective.instant

import com.magicplugin.spells.effective.EffectiveSpell

/**
 * Created by metaheavens on 15/06/17.
 */
abstract class AbstractInstantSpell : EffectiveSpell()
