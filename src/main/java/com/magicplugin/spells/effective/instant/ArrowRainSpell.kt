package com.magicplugin.spells.effective.instant

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.ArrowRainConfig
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Arrow
import org.bukkit.entity.Entity
import org.bukkit.entity.EntityType
import org.bukkit.event.player.PlayerEvent
import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.util.Vector

import java.util.HashSet
import java.util.Random

/**
 * Created by pamartn on 6/7/17.
 */
class ArrowRainSpell(event: PlayerEvent) : AbstractInstantSpell() {

    override val manaPrice: Int
        get() = ArrowRainConfig.MANAPRICE

    init {
        this.cast(event)
    }

    override fun cast(event: PlayerEvent) {
        if (reduceMana(event)) {
            for (x in -WIDTH until WIDTH) {
                for (z in -WIDTH until WIDTH) {
                    val target = event.player.getTargetBlock(null as HashSet<Material>?, 50).location
                    val l = event.player.location
                    val arrow = event.player.world.spawn<Entity>(l.add(l.direction.multiply(1)).add(x.toDouble(), 1.0, z.toDouble()), EntityType.ARROW.entityClass as Class<Entity>) as Arrow
                    val distance = l.distance(target)
                    var v = event.player.location.direction
                    v = v.multiply(Vector(2, 1, 2)).add(Vector(0.0, distance / 70, 0.0))

                    arrow.velocity = v
                    arrow.setMetadata(EXPLOSIVE, FixedMetadataValue(JavaPlugin.getProvidingPlugin(PluginRoot::class.java), ArrowRainConfig.EXPLOSION_RADIUS))
                    arrow.shooter = event.player
                }
            }
        }
    }

    companion object {

        val NAME = ArrowRainConfig.NAME
        var EXPLOSIVE = ArrowRainConfig.EXPLOSIVE
        private val WIDTH = ArrowRainConfig.WIDTH

        val spellFactory: InstantSpellFactory
            get() = object : InstantSpellFactory() {

                override val spellName: String
                    get() = ArrowRainSpell.NAME

                override val spellCost: Int?
                    get() = ArrowRainConfig.MANAPRICE

                override fun launchSpell(event: PlayerEvent): AbstractInstantSpell {
                    return ArrowRainSpell(event)
                }
            }
    }

}
