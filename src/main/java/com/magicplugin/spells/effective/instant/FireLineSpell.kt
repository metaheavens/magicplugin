package com.magicplugin.spells.effective.instant

import com.magicplugin.spells.config.FireLineConfig
import com.magicplugin.tools.BlockTools
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.player.PlayerEvent
import org.bukkit.util.Vector
import kotlin.math.abs

class FireLineSpell private constructor(event: PlayerEvent) : AbstractInstantSpell() {

    override val manaPrice: Int
        get() = FireLineConfig.MANAPRICE

    init {
        this.cast(event)
    }

    public override fun cast(event: PlayerEvent) {
        if (reduceMana(event)) {
            val location = event.player.location
            val direction = event.player.location.direction
            location.add(direction.clone().multiply(2))
            for (i in 0..9) {
                val surface = BlockTools.getSurface(location.add(direction).block)
                surface.location.add(0.0, 1.0, 0.0).block.type = Material.FIRE
            }
        }
    }

    companion object {

        val spellFactory: InstantSpellFactory
            get() = object : InstantSpellFactory() {

                override val spellName: String
                    get() = FireLineConfig.NAME

                override val spellCost: Int?
                    get() = FireLineConfig.MANAPRICE

                override fun launchSpell(event: PlayerEvent): AbstractInstantSpell {
                    return FireLineSpell(event)
                }
            }
    }
}
