package com.magicplugin.spells.effective.instant

import com.magicplugin.spells.effective.EffectiveSpellFactory
import com.magicplugin.spells.effective.castable.AbstractCastableSpell
import org.bukkit.event.player.PlayerEvent

/**
 * Created by metaheavens on 15/06/17.
 */
abstract class InstantSpellFactory : EffectiveSpellFactory() {
    abstract override fun launchSpell(event: PlayerEvent): AbstractInstantSpell

}
