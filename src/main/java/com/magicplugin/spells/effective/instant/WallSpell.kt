package com.magicplugin.spells.effective.instant

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.WallSpellConfig
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.event.player.PlayerEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.util.Vector

import java.util.*

class WallSpell private constructor(event: PlayerEvent) : AbstractInstantSpell() {

    private val material = Material.GLASS
    private val blocks: HashMap<Location, Material> = HashMap()

    /*
        Configuration related attributes
     */
    private val canOverwrite = listOf(Material.AIR, Material.DIRT)
    private val height = 3
    private val width = 4
    private val duration = 30

    override val manaPrice: Int
        get() = WallSpellConfig.MANAPRICE

    init {
        this.cast(event)
    }


    public override fun cast(event: PlayerEvent) {
        if (reduceMana(event)) {
            val location = event.player.getTargetBlock(null as HashSet<Material>?, 50).location
            val direction = event.player.location.direction
            val x = Math.abs(direction.x)
            val z = Math.abs(direction.z)
            val b = location.block
            for (i in -width / 2 until width - width / 2) {
                for (y in 1 until height + 1) {
                    val l = b.location
                    if (z > 0.25 && z < 0.75 || x > 0.25 && x < 0.75) {
                        // We have a diag
                        if (direction.x * direction.z < 0)
                            l.add(i.toDouble(), y.toDouble(), i.toDouble())
                        else
                            l.add((width - width / 2 - i).toDouble(), y.toDouble(), i.toDouble())
                    } else {
                        if (x < z)
                            l.add(i.toDouble(), y.toDouble(), 0.0)
                        else
                            l.add(0.0, y.toDouble(), i.toDouble())
                    }
                    if (canOverwrite.contains(l.block.type)) {
                        blocks[l] = l.block.type
                        l.block.type = material
                    }
                }
            }
            SpellRunnable(this).runTaskLater(JavaPlugin.getProvidingPlugin(PluginRoot::class.java), duration.toLong())
        }
    }


    protected inner class SpellRunnable internal constructor(private var spell: WallSpell) : BukkitRunnable() {

        override fun run() {
            for (l in spell.blocks.keys) {
                l.block.type = spell.blocks[l]!!
            }
            spell.blocks.clear()
        }
    }

    companion object {

        val spellFactory: InstantSpellFactory
            get() = object : InstantSpellFactory() {

                override val spellName: String
                    get() = WallSpellConfig.NAME

                override val spellCost: Int?
                    get() = WallSpellConfig.MANAPRICE

                override fun launchSpell(event: PlayerEvent): AbstractInstantSpell {
                    return WallSpell(event)
                }
            }
    }

}
