package com.magicplugin.spells.passive

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent

class PassiveManager(private val castItem: Material, private val changeItem: Material, private val spell: PassiveSpellActivator) : Listener {

    @EventHandler
    fun onPlayerEvent(event: PlayerInteractEvent) {
        val leftClick = event.action == Action.LEFT_CLICK_AIR || event.action == Action.LEFT_CLICK_BLOCK
        val rightClick = event.action == Action.RIGHT_CLICK_AIR || event.action == Action.RIGHT_CLICK_BLOCK

        if (event.item != null && event.item!!.type == castItem && rightClick) {
            spell.startSpell(event)
        } else if (event.item != null && event.item!!.type == changeItem && leftClick) {
            spell.stopSpell(event.player)
        }
    }
}
