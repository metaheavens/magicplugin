package com.magicplugin.spells.passive

import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerEvent

/**
 * Created by metaheavens on 14/06/17.
 */
abstract class PassiveSpellActivator {
    abstract val spellName: String
    abstract fun startSpell(event: PlayerEvent)
    abstract fun stopSpell(player: Player)
}
