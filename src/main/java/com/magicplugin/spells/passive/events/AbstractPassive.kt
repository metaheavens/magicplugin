package com.magicplugin.spells.passive.events

import com.magicplugin.spells.AbstractSpell
import com.magicplugin.spells.passive.PassiveSpellActivator
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerEvent

abstract class AbstractPassive<T : PlayerEvent> protected constructor(protected val startEvent: PlayerEvent) : AbstractSpell(), Listener {

    open fun handleEvent(handledEvent: T) {
        if (activationCondition(handledEvent)) cast(handledEvent)
    }

    abstract fun activationCondition(handledEvent: T): Boolean

    companion object {

        val spellFactory: PassiveSpellActivator
            @Throws(Exception::class)
            get() = throw Exception("AbstractCastableSpell is not instantiable, he does not have factory.")
    }

}
