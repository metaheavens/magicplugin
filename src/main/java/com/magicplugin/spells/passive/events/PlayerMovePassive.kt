package com.magicplugin.spells.passive.events

import org.bukkit.event.EventHandler
import org.bukkit.event.player.PlayerEvent
import org.bukkit.event.player.PlayerMoveEvent


abstract class PlayerMovePassive
/**
 * We are forced to create class for each generic type of AbstractPassive because Spigot api does not work
 * with generic event handler. The [.handleEvent] method have to be override.
 */
protected constructor(startEvent: PlayerEvent) : AbstractPassive<PlayerMoveEvent>(startEvent) {

    @EventHandler
    override fun handleEvent(handledEvent: PlayerMoveEvent) {
        super.handleEvent(handledEvent)
    }
}
