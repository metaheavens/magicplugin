package com.magicplugin.spells.passive.implemented

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.FireRoundConfig
import com.magicplugin.spells.passive.PassiveSpellActivator
import com.magicplugin.spells.passive.events.PlayerMovePassive
import com.magicplugin.tools.BlockTools
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.util.Vector

import java.util.HashMap

class FireRoundPassive private constructor(startEvent: PlayerEvent) : PlayerMovePassive(startEvent) {

    override val manaPrice: Int
        get() = FireRoundConfig.MANAPRICE

    public override fun cast(event: PlayerEvent) {
        for (x in -FireRoundConfig.RADIUS until FireRoundConfig.RADIUS) {
            for (y in -FireRoundConfig.RADIUS until FireRoundConfig.RADIUS) {
                var loc = event.player.location
                loc.add(Vector(x, -1, y))
                val block = BlockTools.getSurface(loc.block)
                loc = block.location
                loc.add(0.0, 1.0, 0.0)
                loc.block.type = Material.FIRE
            }
        }
    }

    override fun activationCondition(handledEvent: PlayerMoveEvent): Boolean {
        return startEvent.player.name == handledEvent.player.name
    }

    private class FireRoundActivator() : PassiveSpellActivator() {
        private val playerToListener: HashMap<Player, Listener> = HashMap()

        override val spellName: String
            get() = FireRoundConfig.NAME

        override fun startSpell(event: PlayerEvent) {
            if (!playerToListener.containsKey(event.player)) {
                event.player.server.broadcastMessage("START SPELL")
                val fireRoundPassive = FireRoundPassive(event)
                JavaPlugin.getProvidingPlugin(PluginRoot::class.java).server.pluginManager.registerEvents(fireRoundPassive, JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
                playerToListener[event.player] = fireRoundPassive
            }
        }

        override fun stopSpell(player: Player) {
            player.server.broadcastMessage("STOP SPELL")
            playerToListener[player]?.let { PlayerMoveEvent.getHandlerList().unregister(it) }
            playerToListener.remove(player)
        }
    }

    companion object {
        val spellFactory: PassiveSpellActivator
            get() = FireRoundActivator()
    }
}
