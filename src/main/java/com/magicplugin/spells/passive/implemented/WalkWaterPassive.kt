package com.magicplugin.spells.passive.implemented

import com.magicplugin.PluginRoot
import com.magicplugin.spells.config.WalkWaterConfig
import com.magicplugin.spells.passive.PassiveSpellActivator
import com.magicplugin.spells.passive.events.PlayerMovePassive
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.util.Vector

import java.util.HashMap

class WalkWaterPassive private constructor(startEvent: PlayerEvent) : PlayerMovePassive(startEvent) {

    protected override val manaPrice: Int
        get() = WalkWaterConfig.MANAPRICE

    public override fun cast(event: PlayerEvent) {
        for (x in -WalkWaterConfig.RADIUS until WalkWaterConfig.RADIUS) {
            for (y in -WalkWaterConfig.RADIUS until WalkWaterConfig.RADIUS) {
                val loc = event.player.location
                loc.add(Vector(x, -1, y))
                val block = loc.block
                if (block.type == Material.WATER && reduceMana(event)) {
                    block.type = Material.FROSTED_ICE
                }
            }
        }
    }

    override fun activationCondition(handledEvent: PlayerMoveEvent): Boolean {
        return startEvent.player.name == handledEvent.player.name
    }

    private class WalkWaterActivator() : PassiveSpellActivator() {
        private val playerToListener: HashMap<Player, Listener> = HashMap()

        override val spellName: String
            get() = WalkWaterConfig.NAME

        override fun startSpell(event: PlayerEvent) {
            if (!playerToListener.containsKey(event.player)) {
                event.player.server.broadcastMessage("START SPELL")
                val walkWaterPassive = WalkWaterPassive(event)
                JavaPlugin.getProvidingPlugin(PluginRoot::class.java).server.pluginManager.registerEvents(walkWaterPassive, JavaPlugin.getProvidingPlugin(PluginRoot::class.java))
                playerToListener[event.player] = walkWaterPassive
            }
        }

        override fun stopSpell(player: Player) {
            player.server.broadcastMessage("STOP SPELL")
            PlayerMoveEvent.getHandlerList().unregister(playerToListener[player]!!)
            playerToListener.remove(player)
        }
    }

    companion object {

        val spellFactory: PassiveSpellActivator
            get() = WalkWaterActivator()
    }
}
