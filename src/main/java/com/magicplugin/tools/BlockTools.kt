package com.magicplugin.tools

import org.bukkit.Location
import org.bukkit.block.Block

object BlockTools {
    fun getSurface(blockIn: Block): Block {
        var block = blockIn
        val l = block.location
        l.y = 255.0
        block = l.block
        var i = 255
        while (!block.type.isSolid || block.type.isBurnable) {
            val loc = block.location
            loc.y = i.toDouble()
            block = loc.block
            i--
        }
        return block
    }
}
