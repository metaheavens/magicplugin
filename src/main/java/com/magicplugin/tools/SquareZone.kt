package com.magicplugin.tools

import org.bukkit.Location
import org.bukkit.block.Block

import java.util.ArrayList

class SquareZone(private val firstLocation: Location, private val secondLocation: Location) {
    private val minX: Double
    private val maxX: Double
    private val minZ: Double
    private val maxZ: Double

    val surface: ArrayList<Block>
        get() {
            val result = ArrayList<Block>()

            var i = minX
            while (i <= maxX) {
                var j = minZ
                while (j <= maxZ) {
                    result.add(BlockTools.getSurface(firstLocation.world!!.getBlockAt(i.toInt(), 255, j.toInt())))
                    j++
                }
                i++
            }
            return result
        }

    init {
        minX = if (firstLocation.x <= secondLocation.x) firstLocation.x else secondLocation.x
        maxX = if (firstLocation.x <= secondLocation.x) secondLocation.x else firstLocation.x
        minZ = if (firstLocation.z <= secondLocation.z) firstLocation.z else secondLocation.z
        maxZ = if (firstLocation.z <= secondLocation.z) secondLocation.z else firstLocation.z
    }

    fun isInZone(l: Location): Boolean {
        val isInXZone = l.x >= minX && l.x <= maxX
        val isInZZone = l.z >= minZ && l.z <= maxZ

        return isInXZone && isInZZone
    }
}
